import subprocess, os, time
print ('Start testing')
testRunTime = 10

# start the stress test in parallel
subprocess.Popen('python3 cpu_stress.py', shell=True)
time.sleep(testRunTime)
# Kill the stress test process
subprocess.Popen('pkill -9 -f cpu_stress.py', shell=True)
print("Stress Test Completed")
